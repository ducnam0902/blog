@extends('master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 mx-auto my-5 form gradient">
            <h3 class="text-center my-3">Update User</h3>
            <form action="" method="POST">
                @csrf
                <h3>Email</h3>
                <input class="form-control" type="email" name="email"  maxlength="50" required @if (!empty($user)) value="{{$user->email}}" @endif>
                @error('email')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                <h3>Phone</h3>
                <input class="form-control" type="text" name="phone" @if (!empty($user)) value="{{$user->phone}}" @endif>
                @error('phone')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                <h3>Role</h3>
                <select class="form-control" name="role" >
                    <option value="0">Thành viên</option>
                    <option value="1">Admin</option>
                </select>
                <button type="submit" class="btn btn-secondary d-block mx-auto my-3">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection