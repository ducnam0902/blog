<!DOCTYPE html>
<html lang="en">

<head>
	<title>Change Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/animate/animate.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
	<!--===============================================================================================-->
</head>

<body>


	<div class="container-login100" style="background-image: url({{asset('/assets/images/bg2.jpg')}})">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-50 p-b-30">
			<form action="" method="POST" class="login100-form validate-form">
				@csrf
				<span class="login100-form-title p-b-20">
					Change Password
				</span>
				<div class="wrap-input100 m-b-20">
					<p class="input100 pt-4 my-0" >@if (!empty($user)) {{$user->email}} @endif</p>
					<span class="focus-input100"></span>
				</div>
				
				<div class="wrap-input100 m-b-20" >
					<input class="input100" type="password" name="oldpassword" placeholder="Old password">
					<span class="focus-input100"></span>
				</div>
				@error('oldpassword')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
				<div class="wrap-input100 m-b-25" data-validate="Enter password">
					<input class="input100" type="password" name="newpassword" placeholder=" New Password">
					<span class="focus-input100"></span>
				</div>
				@error('newpassword')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
				<div class="wrap-input100 m-b-25" data-validate="Enter phone">
					<input class="input100" type="password" name="confirmpassword" placeholder="Confirm new password">
					<span class="focus-input100"></span>
				</div>
				@error('confirmpassword')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
				<p class="input100 text-center" >@if (!empty($text)) {{$text}} @endif</p>
				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit">
						Confirm
					</button>
				</div>

				<div class="text-center my-3">
					<a href="{{route('login')}}" class="txt2 hov1">
						Sign In
					</a>
				</div>
			</form>


		</div>
	</div>



	<div id="dropDownSelect1"></div>

	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/animsition/js/animsition.min.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/select2/select2.min.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
	<script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/vendor/countdowntime/countdowntime.js')}}"></script>
	<!--===============================================================================================-->
	<script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>