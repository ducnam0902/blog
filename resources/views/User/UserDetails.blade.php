@extends('master')
@section('content')
<div class="container ">
<div class="row ">
    <div class="col-sm-12">
        <h2 class="text-center my-3">Thành viên</h2>
    </div>
</div>
<div class="row my-4">
    <div class="col-sm-6">
        <form action="{{route('searchUser')}}" method="GET">
            <input type="search" class="form-control " name="keyword">
    </div>
    <div class="col-sm-3 pl-1">
        <button class="btn btn-secondary " type="submit">Tìm kiếm</button>
    </div>

    </form>

    <div class="col-sm-3 text-right">
        <a class="btn btn-primary text-center mr-auto" href="{{route('insertUser')}}">Thêm mới</a>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table text-center">
            <thead class="">
                <tr>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Emall</th>
                    <th>Số điện thoại</th>
                    <th>Vị trí</th>
                    <th>Cập nhật</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($users))
                @foreach($users as $user)
                <tr>
                    <td>{{$user -> id}}</td>
                    <td>{{$user -> name}}</td>
                    <td>{{$user -> email}}</td>
                    <td>{{$user ->phone}}</td>
                    <td>{{($user -> role == '0')?'Thành viên':'Admin'}}</td>
                    <td>
                        <a class="btn btn-success btn-sm" href="{{route('updateUser',$user -> id)}}">Cập nhật</a> |
                        <a class="btn btn-danger btn-sm" href="{{route('deleteUser',$user -> id)}}">Xóa</a> |
                        <a href="{{route('changePassword', $user->id)}}" class="btn btn-secondary">Đổi mật khẩu</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
</div>
</div>

@endsection