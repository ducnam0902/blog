@extends('master')
@section('content')
<div class="container-fluid mb-0">
    <div class="row ">
        <div class="col-sm-4 mx-auto form my-4 pb-4 gradient">
            <h1 class="text-center my-3 mx-3">Thêm thành viên mới</h1>
            <form action="{{route('insertUser')}}" method="POST">
                @csrf
                <div class="form-group">
                    <h4 class="form-controll ">Tên</h4>
                    <input class="form-control" type="text" name="name" pattern="[A-Za-z0-9]+" maxlength="50" required>
                    @error('name')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <h4>Email</h4>
                    <input class="form-control" type="email" name="email">
                    @error('email')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <h4>Mật khẩu</h4>
                    <input class="form-control" type="password" name="password">
                    @error('password')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <h4>Số điện thoại</h4>
                    <input class="form-control" type="text" name="phone">
                    @error('phone')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <button class="btn btn-secondary my-3  d-block mx-auto" type="submit">Thêm mới</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection