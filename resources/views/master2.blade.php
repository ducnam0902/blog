<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Poo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="{{asset('assets/home/vendor/bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="{{asset('assets/home/vendor/font-awesome/css/font-awesome.min.css')}}">
  <!-- Custom icon font-->
  <link rel="stylesheet" href="{{asset('assets/home/css/fontastic.css')}}">
  <!-- Google fonts - Open Sans-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <!-- Fancybox-->
  <link rel="stylesheet" href="{{asset('assets/home/vendor/@fancyapps/fancybox/jquery.fancybox.min.css')}}">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="{{asset('assets/home/css/style.default.css')}}" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="{{asset('assets/home/css/custom.css')}}">
  <!-- Favicon-->
  <link rel="shortcut icon" href="{{asset('assets/home/favicon.png')}}">
	<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
  <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
</head>

<body>
  {{--Header--}}
  @include('core.header2')
  {{--End Header--}}
  {{--Container--}}
  @yield('content2')

  {{--Footer--}}
  @include('core.footer2')
  {{--End Footer--}}
  {{--End Container--}}

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
</body>

</html>