<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Admin</title>
    <link href="{{asset('assets/admin/dist/css/styles.css')}}" rel="stylesheet" />
    <!-- <script src="//cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('ckeditor/ckfinder/ckfinder.js')}}"></script>
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body>
    {{--Header--}}
    @include('core.header')
    {{--End Header--}}
    {{--Container--}}
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark " id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Core</div>
                        <a class="nav-link" href="{{route('home')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Trang chủ
                        </a>
                        <div class="sb-sidenav-menu-heading">Quản lý</div>
                        <a class="nav-link collapsed" href="{{route('category')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Chuyên đề

                        </a>
                        <a class="nav-link collapsed" href="{{route('post')}}" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                            Bài viết
                        </a>
                        <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                @if(!empty($categories))
                                @foreach($categories as $category)
                                <a class="nav-link" href="{{route('postFollow',$category->id)}}">{{$category->name}}</a>
                                @endforeach
                                @endif
                                <a class="nav-link" href="{{route('post')}}">Tạo bài viết</a>
                                <!-- <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a> -->
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="{{route('user')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Thành viên
                        </a>
                    </div>
                    <div class="sb-sidenav-footer pt-10">
                        <div class="small">Logged in as:</div>
                        {{strtolower(auth()->user()->name)}}
                    </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main class="user ">
                @yield('content')
            </main>
            {{--Footer--}}
            @include('core.footer')
            {{--End Footer--}}
        </div>
    </div>
    {{--End Container--}}
    <script>
        CKEDITOR.replace('ckeditor1', {
            height: 300,
            filebrowserBrowseUrl: "{{ asset('ckfinder/ckfinder.html') }}",
            filebrowserImageBrowseUrl: "{{ asset('ckfinder/ckfinder.html?type=Images') }}",
            filebrowserFlashBrowseUrl: "{{ asset('ckfinder/ckfinder.html?type=Flash') }}",
            filebrowserUploadUrl: "{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}",
            filebrowserImageUploadUrl: "{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}",
            filebrowserFlashUploadUrl: "{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}"
        });
    </script>
    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{asset('assets/admin/dist/js/scripts.js')}}"></script>
</body>

</html>