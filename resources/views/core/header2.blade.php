<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class=" d-flex align-items-center justify-content-between navbar-brand" href="{{route('home')}}"><img src="{{asset('assets/home/img/logo.png')}}" width="100px" height="60px" alt=""></a>
        <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="true" aria-label="Toggle navigation"><span></span><span></span><span></span></button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('home')}}">Trang chủ <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#"  id="dropdownId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId2">
                    @if(!empty($categories))
                    @foreach($categories as $category)
                        <a class="dropdown-item" href="{{route('blogcategory',$category->slug)}}">{{$category -> name}}</a>
                    @endforeach
                    @endif
                    </div>
                </li>
                @if(!empty(auth()->user()))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{strtolower(auth()->user()->name)}}</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="{{route('changePassword',auth()->user()->id)}}">Đổi mật khẩu</a>
                        <a class="dropdown-item" href="{{route('logout')}}">Đăng xuất</a>
                    </div>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Login</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>