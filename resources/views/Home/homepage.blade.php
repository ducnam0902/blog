@extends('master2')
@section('content2')
<!-- Hero Section-->
<section style="background: url(assets/home/img/title.jpg); background-size: cover; background-position: center; " class="hero img-responsive">
  <div class="container">
    <div class="row">
      <div class="col-lg-10">
      <h1></h1>
      </div>
    </div>
  </div>
</section>
<!-- Intro Section-->
<section class="intro">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <h2 class="h3"></h2>
        <p class="text-big">“Hãy thử <strong>quên đi mình là ai </strong> và làm những gì mà mình <strong> mong muốn ”</strong></p>
        <p class="text-big font-weight-bold text-right"> - Ngọn lửa đức hạnh -</p>
      </div>
    </div>
  </div>
</section>
<section class="featured-posts no-padding-top">
  <div class="container">
    @if(!empty($news))
    @foreach($news as $new)
    <!-- Post-->
      @if($loop->odd)
    <div class="row d-flex align-items-stretch">
      <div class="text col-lg-7">
        <div class="text-inner d-flex align-items-center">
          <div class="content">
            <header class="post-header">
              <div class="category"><a href="{{route('clientPost',$new->slug)}}">{{$new ->category->name}}</a></div><a href="{{route('clientPost',$new->slug)}}">
                <h2 class="h4">{{$new ->title}}</h2>
              </a>
            </header>
            <p>{{$new ->subtitle}}.</p>
            <footer class="post-footer d-flex align-items-center"><a href="{{route('clientPost',$new->slug)}}" class="author d-flex align-items-center flex-wrap">
                <div class="title"><span>{{date_format($new->created_at,"d-m-Y")}}</span></div>
              </a>
            </footer>
          </div>
        </div>
      </div>
      <div class="image col-lg-5"><img src="{{asset('assets/images/'.$new->image)}}" class="img-responsive" alt="..."></div>
    </div>
      @else
    <!-- Post        -->
    <div class="row d-flex align-items-stretch">
      <div class="image col-lg-5"><img src="{{asset('assets/images/'.$new->image)}}" class="img-responsive" alt="..."></div>
      <div class="text col-lg-7">
        <div class="text-inner d-flex align-items-center">
          <div class="content">
            <header class="post-header">
              <div class="category"><a href="{{route('clientPost',$new->slug)}}">{{$new ->category->name}}</a></div><a href="{{route('clientPost',$new->slug)}}">
                <h2 class="h4">{{$new ->title}}</h2>
              </a>
            </header>
            <p>{{$new ->subtitle}}.</p>
            <footer class="post-footer d-flex align-items-center"><a href="{{route('clientPost',$new->slug)}}" class="author d-flex align-items-center flex-wrap">
                <div class="title"><span>{{date_format($new->created_at,"d-m-Y")}}</span></div>
              </a>
            </footer>
          </div>
        </div>
      </div>
    </div>
      @endif
    @endforeach
    @endif
  </div>
</section>
<!-- Divider Section-->
<section style="background: url(assets/home/img/3.png); background-size: cover; background-position: center center" class="divider">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h2>"Người mình cho là tốt nhưng nhiều khi lại không phải vậy!"
        </h2>
        <p class="hero-link"> - Ngọn lửa đức hạnh - </p>
      </div>
    </div>
  </div>
</section>
<!-- Latest Posts -->
<section class="latest-posts">
  <div class="container">
    <header>
      <h2>Mới nhất từ Blog</h2>
    </header>
    <div class="row">
      @if(!empty($news))
      @foreach($news as $new)
      <div class="post col-md-4">
        <div class="post-thumbnail"><a href="{{route('clientPost',$new->slug)}}"><img src="{{asset('assets/images/'.$new->image)}}" width="350px" height="200px" alt="..." class="img-responsive"></a></div>
        <div class="post-details">
          <div class="post-meta d-flex justify-content-between">
            <div class="date">{{date_format($new->created_at,"d-m-Y")}}</div>
            <div class="category"><a href="{{route('clientPost',$new->slug)}}">{{$new ->category->name}}</a></div>
          </div><a href="{{route('clientPost',$new->slug)}}">
            <h3 class="h4">{{$new ->title}}</h3>
          </a>
          <p class="text-muted">{{$new ->subtitle}}</p>
        </div>
      </div>
      @endforeach
      @endif

    </div>
  </div>
</section>
<!-- Newsletter Section-->
<!-- Gallery Section-->
<section class="gallery no-padding">
  <div class="row">
    <div class="mix col-lg-3 col-md-3 col-sm-6">
      <div class="item"><img src="assets/home/img/title1.jpg" alt="..." class="h-100 w-100">
        <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div>
        </a>
      </div>
    </div>
    <div class="mix col-lg-3 col-md-3 col-sm-6">
      <div class="item"><img src="assets/home/img/title2.jpg" alt="..." class="h-100 w-100">
        <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div>
        </a>
      </div>
    </div>
    <div class="mix col-lg-3 col-md-3 col-sm-6">
      <div class="item"><img src="assets/home/img/title3.jpg" alt="..." class="h-100 w-100">
        <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div>
        </a>
      </div>
    </div>
    <div class="mix col-lg-3 col-md-3 col-sm-6">
      <div class="item"><img src="assets/home/img/title4.jpg" alt="..." class="h-100 w-100">
        <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection