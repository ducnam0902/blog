@extends('master')
@section('content')
<div class="container-fluid mb-0">
    <div class="row ">
        <div class="col-sm-8 mx-auto form my-5 pb-4 gradient">
            <h1 class="text-center my-3 mx-3">Add new Category</h1>
            <form action="{{route('uploadform')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <h4 class="form-controll ">Tên</h4>
                    <input class="form-control" type="text" name="name" maxlength="50" required>
                    @error('name')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
                    @enderror
                    <h4 class="form-controll ">Slug</h4>
                    <input class="form-control" type="text" name="slug" maxlength="50" required>
                    @error('slug')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
                    @enderror
                    <h4>Hình ảnh</h4>
                    <input type="file" name="image" class="form-control">
                    @error('image')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
                    @enderror
                    <button class="btn btn-secondary my-3  d-block mx-auto" type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection