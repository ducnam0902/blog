@extends('master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 mx-auto my-5 form gradient">
            <h3 class="text-center my-3">Update Category</h3>
            <form action="{{route('updateCategory',$category->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <h3>Tên</h3>
                <input class="form-control" type="text" name="name" maxlength="50" required @if (!empty($category)) value="{{$category->name}}" @endif>
                <h4 class="form-controll ">Slug</h4>
                <input class="form-control" type="text" name="slug" maxlength="50" required @if (!empty($category)) value="{{$category->slug}}" @endif>
                <h3>Hình ảnh</h3>
                <input type="file" name="image" class="form-control">
                <button type="submit" class="btn btn-secondary d-block mx-auto my-3">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection