@extends('master')
@section('content')
<div class="container ">
<div class="row ">
    <div class="col-sm-12">
        <h2 class="text-center my-3">Chuyên đề</h2>
    </div>
</div>
<div class="row my-4">
   

    <div class="col-sm-3 text-left">
        <a class="btn btn-primary text-center mr-auto" href="{{route('insertCategory')}}">Add</a>
    </div>

</div>

<div class="row">
    <div class="col-sm-12 ">
        <table class="table text-center">
            <thead class="">
                <tr>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Hình ảnh</th>
                    <th>Slug</th>
                    <th>Cập nhật</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($categories))
                @foreach($categories as $category)
                <tr>
                    <td>{{$category -> id}}</td>
                    <td>{{$category -> name}}</td>
                    <td> <img src="assets/images/{{$category -> image}}" width="200px" height="100px" alt=""></td>
                    <td>{{$category -> slug}}</td>
                    <td>
                        <a class="btn btn-success btn-sm" href="{{route('updateCategory',$category -> id)}}">Cập nhật</a> |
                        <a class="btn btn-danger btn-sm" href="{{route('deleteCategory',$category -> id)}}">Xóa</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection