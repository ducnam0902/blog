@extends('master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 mx-auto my-5 form gradient">
            <h3 class="text-center my-3">Cập nhật bài viết</h3>
            <form action="{{route('updatepost',$post->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <h4 class="form-controll">Tiêu đề</h4>
                <input class="form-control" type="text" name="title" @if (!empty($post)) value="{{$post->title}}" @endif required>
                <h4 class="form-controll">Tiêu đề phụ</h4>
                <input class="form-control" type="text" name="subtitle" @if (!empty($post)) value="{{$post->subtitle}}" @endif required></input>
                <h4 class="form-controll">Slug</h4>
                <input type="text" name="slug" @if (!empty($post)) value="{{$post->slug}}" @endif class="form-control">
                <h4 class="form-controll">Hình ảnh</h4>
                <input type="file" name="image" class="form-control" value="1">
                <h4 class="form-controll">Mô tả</h4>
                <textarea class="form-control" name="description" id="ckeditor1">{{$post->description}}</textarea>
                <h4 class="form-controll">Chuyên đề</h4>
                <select class="form-control" name="category_id" id="" @if (!empty($post)) value="{{$post->category->name}}" @endif>
                    @if(!empty($categories))
                    @foreach($categories as $category)
                    <option @if ($post->category_id == $category->id ) selected  @endif value="{{$category->id}}">{{$category -> name}}</option>
                    @endforeach
                    @endif
                </select>
                <button type="submit" class="btn btn-secondary d-block mx-auto my-3">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
