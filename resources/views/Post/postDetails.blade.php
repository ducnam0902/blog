@extends('master')
@section('content')
<div class="container ">
    <div class="row ">
        <div class="col-sm-12">
            <h2 class="text-center my-3">Bài viết</h2>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-sm-6">
            <form action="{{route('searchpost')}}" method="GET">
                <input type="search" class="form-control " name="keyword">
        </div>
        <div class="col-sm-3 pl-1">
            <button class="btn btn-secondary " type="submit">Search</button>
        </div>

        </form>

        <div class="col-sm-3 text-right">
            <a class="btn btn-primary text-center mr-auto" href="{{route('insertpost')}}">Add</a>
        </div>

    </div>
    <div class="row" style="font-size:14px">
        <div class="col-sm-12">
            <table class="table text-center">
                <thead class="thead-dark">
                    <tr class="d-flex">
                        <th class="col-1">Id</th>
                        <th class="col-2">Tiêu đề</th>
                        <th class="col-1">Tiêu đề phụ</th>
                        <th class="col-3">Hình ảnh</th>
                        <th class="col-1">Mô tả</th>
                        <th class="col-1">Slug</th>
                        <th class="col-1">Chuyên mục</th>
                        <th class="col-2">Cập nhật</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($posts))
                    @foreach($posts as $post)
                    <tr class="d-flex text-center">
                        <td class="col-1">{{$post -> id}}</td>
                        <td class="col-2">{{$post -> title}}</td>
                        <td class="col-1"><textarea disabled name="" id="" cols="10" rows="5">{{$post -> subtitle}}</textarea></td>
                        <td class="col-3"> <img src="{{asset('assets/images/'.$post -> image)}}" width="200px" height="110px" alt=""></td>
                        <td class="col-1"><textarea disabled name="" id="" cols="10" rows="5">{{$post -> description}}</textarea></td>
                        <td class="col-1">{{$post->slug}}</td>
                        <td class="col-1">{{$post -> category -> name}}</td>
                        <td >
                            <a class="btn btn-success btn-sm" href="{{route('updatepost',$post -> id)}}">Cập nhật</a>
                        </td>
                        <td> <a class="btn btn-danger btn-sm" href="{{route('deletepost',$post -> id)}}">Xóa</a></td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
            {{$posts->links()}}
        </div>
    </div>
</div>
@endsection
