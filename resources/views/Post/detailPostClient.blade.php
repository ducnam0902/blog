@extends('master2')
@section('content2')
<div class="container">
  <div class="row">
    <!-- Latest Posts -->
    <main class="post blog-post col-lg-8">
      <div class="container">
        <div class="post-single">
          <div class="post-thumbnail"><img src="{{asset('assets/images/'.$post->image)}}" alt="..." class="img-fluid"></div>
          <div class="post-details">
            <div class="post-meta d-flex justify-content-between">
              <div class="category">{{$post->category->name}}</div>
            </div>
            <h1>{{$post->title}}<a href="#"><i class="fa fa-bookmark-o"></i></a></h1>
            <div class="post-footer d-flex align-items-center flex-column flex-sm-row">
              <div class="d-flex align-items-center flex-wrap">
                <div class="date"><i class="icon-clock"></i> {{date_format($post->created_at,"d-m-Y")}}</div>
              </div>
            </div>
            <div class="post-body">
              <p class="lead">{{$post->subtitle}}</p>
              <div id="trackingDiv">
                {!!$post->description!!}
              </div>
            </div>
          </div>
        </div>
    </main>
    <aside class="col-lg-4">
      <!-- Widget [Latest Posts Widget]        -->
      <div class="widget latest-posts">
        <header>
          <h3 class="h6">Bài đăng mới nhất</h3>
        </header>
        <div class="blog-posts">
          @if(!empty($news))
          @foreach($news as $new)
          <a href="{{route('clientPost',$new->slug)}}">
            <div class="item d-flex align-items-center">
              <div class="image"><img src="{{asset('assets/images/'.$new->image)}}" alt="..." class="img-fluid"></div>
              <div class="title"><strong>{{$new ->title}}</strong>
              </div>
            </div>
          </a>
          @endforeach
          @endif

        </div>
      </div>
      <!-- Widget [Categories Widget]-->
      <div class="widget categories">
        <header>
          <h3 class="h6">Thể loại</h3>
        </header>
        @if(!empty($categories))
        @foreach($categories as $category)
        <div class="item d-flex justify-content-between"><a href="{{route('blogcategory',$category->slug)}}">{{$category -> name}}</a></div>
        @endforeach
        @endif
      </div>
    </aside>
  </div>
</div>
<script>
  CKEDITOR.replace("{{$post->description}}");
  var editorText = CKEDITOR.instances.editor2.getData();
  $('#trackingDiv').html(editorText);
</script>

@endsection