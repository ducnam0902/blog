@extends('master')
@section('content')
<div class="container-fluid mb-0">
    <div class="row ">
        <div class="col-sm-10 mx-auto form my-5 pb-4 gradient">
            <h1 class="text-center my-3 mx-3">Đăng bài</h1>
            <form action="{{route('uploadformPost')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <h4 class="form-controll">Tiêu đề</h4>
                    <input class="form-control" type="text" name="title"  required>
                    @error('title')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
                    @enderror
                    <h4 class="form-controll">Tiêu đề phụ</h4>
                    <textarea class="form-control" type="text" name="subtitle" required></textarea>
                    <h4 class="form-controll">Hình ảnh</h4>
                    <input type="file" name="image" class="form-control">
                    <h4 class="form-controll">Slug</h4>
                    <input type="text" name="slug" class="form-control">
                    @error('slug')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <h4 class="form-controll">Mô tả</h4>
                    <textarea class="form-control" name="description" id="ckeditor1"></textarea>
                    @error('description')
					<div class="text-danger text-center font-weight-bold">{{ $message }}</div>
					@enderror
                    <h4 class="form-controll">Chuyên đề</h4>
                    <select class="form-control" name="category_id" id="">
                        @if(!empty($categories))
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category -> name}}</option>
                        @endforeach
                        @endif
                    </select>
                    <button class="btn btn-secondary my-3  d-block mx-auto" type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection