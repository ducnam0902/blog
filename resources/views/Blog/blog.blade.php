@extends('master2')
@section('content2')
<div class="container">
    <div class="row">
        <!-- Latest Posts -->
        <main class="posts-listing col-lg-8">
            <div class="container">
                <div class="row">
                    <!-- post -->
                    @if(!empty($posts))
                    @foreach($posts as $post)
                    <div class="post col-xl-6">
                        <div class="post-thumbnail"><a href="{{route('clientPost',$post->slug)}}"><img src="{{asset('assets/images/'.$post->image)}}" width="350px" height="200px" alt="..." class="img"></a></div>
                        <div class="post-details">
                            <div class="post-meta d-flex justify-content-between">
                                <div class="date meta-last">{{date_format($post->created_at,"d-m-Y")}}</div>
                                <div class="category">{{$post ->category->name}}</a></div>
                            </div><a href="{{route('clientPost',$post->slug)}}">
                                <h3 class="h4">{{$post->title}}</h3>
                            </a>
                            <p class="text-muted">{{$post->subtitle}}</p>
                        </div>
                    </div>
                    @endforeach
                    @endif    
                </div>
                {{$posts->links()}}
            </div>
        </main>
        <aside class="col-lg-4">
            <!-- Widget [Latest Posts Widget]        -->
            <div class="widget latest-posts">
                <header>
                    <h3 class="h6">Bài đăng mới nhất</h3>
                </header>
                <div class="blog-posts">
                    @if(!empty($news))
                    @foreach($news as $new)
                    <a href="{{route('clientPost',$new->slug)}}">
                        <div class="item d-flex align-items-center">
                            <div class="image"><img src="{{asset('assets/images/'.$new->image)}}" alt="..." class="img-fluid"></div>
                            <div class="title"><strong>{{$new ->title}}</strong>
                            </div>
                        </div>
                    </a>
                    @endforeach
                    @endif
                </div>
            </div>
            <!-- Widget [Categories Widget]-->
            <div class="widget categories">
                <header>
                    <h3 class="h6">Thể loại</h3>
                </header>
                @if(!empty($categories))
                @foreach($categories as $category)
                <div class="item d-flex justify-content-between"><a href="{{route('blogcategory',$category->slug)}}">{{$category -> name}}</a></div>
                @endforeach
                @endif
            </div>
    </div>
</div>
@endsection