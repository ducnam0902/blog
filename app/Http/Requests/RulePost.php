<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RulePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required||min:5||max:50||regex:/^[\\p{L}\\s]/',
            'subtitle'=>'required||max:200',
            'image'=>'required||image',
            'description'=>'required',
            'category_id' =>'required',
            'slug' =>'required||alpha_dash'
        ];
    }
    public function messages(){
        return [
            'title.required'=>'Yêu cầu tiêu đề',
            'title.min'=>'Tiêu đề phải lớn 5 kí tự',
            'title.regex:/^[\\p{L}\\s]/'=> 'Yêu cầu tiếng Việt',
            'subtitle.required'=>'Yêu cầu tiêu đề phụ',
            'description.required'=> 'Yêu cầu nội dung mô tả',
            'category_id.required' =>'Yêu cầu chuyên mục cho bài viết',
            'slug.required'=>'Yêu cầu đường dẫn thân thiện cho bài viết',
            'slug.alpha_dash'=>'Yêu cầu đường dẫn thân thiện phải có chữ cái dấu gạch ngang hoặc dấu gạch dưới'
        ];
    }
}
