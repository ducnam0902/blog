<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RuleChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldpassword'=>'required|min:5',
            'oldpassword'=>'required|min:5',
            'newpassword'=>'required|min:5',
            'newpassword'=>'required|min:5',
            'confirmpassword'=>'required|min:5',
            'confirmpassword'=>'required|min:5',
        ];
    }
    public function messages()
    {
        return [
            'oldpassword.required'=>'Nhập mật khẩu',
            'oldpassword.min'=>'Mật khẩu lớn hơn 5 ký tự ',
            'newpassword.required'=>'Nhập mật khẩu',
            'newpassword.min'=>'Mật khẩu lớn hơn 5 ký tự ',
            'confirmpassword.required'=>'Nhập mật khẩu',
            'confirmpassword.min'=>'Mật khẩu lớn hơn 5 ký tự ',
        ];
    }
}
