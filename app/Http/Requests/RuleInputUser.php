<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RuleInputUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>'required|email|ends_with:@gmail.com',
            'name'=>'required|min:3|alpha_num',
            'password'=>'required|min:5',
            'phone'=>'required|digits_between:10,10',
        ];
    }
    public function messages()
    {
        return [
            'email.required'=>'Nhập Email',
            'email.email'=>'Định dạng Gmail ',
            'email.ends_with'=>'Kết thúc với @gmail.com',
            'name.required'=>'Nhập username ',
            'name.min'=>'Username phải lớn hơn 3 ký tự ',
            'name.alpha_num'=>'Username chỉ gồm chữ cái và số',
            'password.required'=>'Nhập mật khẩu',
            'password.min'=>'Mật khẩu lớn hơn 5 ký tự ',
            'phone.required'=>'Nhập số điện thoại nhé',
            'phone.digits_between'=>'Chỉ cho phép 10 số'
        ];
    }
}
