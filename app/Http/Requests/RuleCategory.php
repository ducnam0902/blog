<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RuleCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required||min:5||regex:/^[\\p{L}\\s]/||',
            'slug' =>'required||alpha_dash',
            'image' =>'required||image'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Nhập chuyên mục ',
            'name.min'=>'Chuyên mục phải lớn hơn 5 ký tự ',
            'name.regex:/^[a-zA-Z0-9\s]+$/'=>'Chuyên mục chỉ gồm chữ cái và số',
            'slug.required'=>'Nhập đường dẫn thân thiện',
            'slug.alpha_dash'=>'Đường dẫn thân thiện chỉ gồm chữ và dấu gạch',
            'image.required'=>'Chọn ảnh ',
            'image.image'=>'Định dạng là ảnh'
        ];
    }
}
