<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RuleChangePassword;
use App\Http\Requests\RuleInputUser;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    public function login()
    {
        return view('Auth.login');
    }
    public function postlogin(Request $request)
    {
        $user = [
            'name' => $request->name,
            'password' => $request->password
        ];
       
        if (Auth::attempt($user)) {
            return redirect()->route('index');
        } else{
            $message="Name or password not correct.";
            return view('Auth.login',['message' =>$message]);
        }
            
    }
    public function register()
    {
        return view('Auth.register');
    }
    public function postregister(RuleInputUser $request)
    {
        $user = [
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'phone' => $request->phone,
        ];
        $user_registered = User::create($user);
        Auth::login($user_registered);
        return redirect()->route('index');
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
    public function editPassword($id){
        $user = User::find($id); 
        return view('User.changePassword',
                [
                    'user'=> $user
                ]);
    }
    public function updatePassword(RuleChangePassword $request){
        $validated = $request->validated();
        $user = User::find($request->id); 
        $oldPassword = $request -> oldpassword;
        $newPassword = $request -> newpassword;
        $confirmpassword= $request -> confirmpassword;
        $userpassword= $user->password;

        if(Hash::check($oldPassword,$userpassword) && $oldPassword!= $newPassword && $newPassword== $confirmpassword)
        {
            $text= 'Successfull change password';
            $user -> password= Hash::make($newPassword);
            $user ->save();
            Auth::logout();
            return view('User.changePassword',
            [
                'user'=> $user,
                'text'=> $text
            ]);
        }
        else
        {
            $text='Failed change password.Please check password again';
            return view('User.changePassword',
            [
                'user'=> $user,
                'text'=> $text
            ]);
        }
            
    }
}
