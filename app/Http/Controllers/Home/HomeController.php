<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\PostEloquentRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $postRepository;
    protected $categoryRepository;
    public function __construct(PostEloquentRepository $postRepository, CategoryEloquentRepository $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        
    }
    function index(){
        $news = $this->postRepository->getLastestNew();
        $categories = $this->categoryRepository->all();
        return view('Home.homepage',[ 'news' =>$news,'categories'=>$categories]);
    }
}
