<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\RuleCategory;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $categoryRepository;
    public function __construct(CategoryEloquentRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('Category.detailsCategory', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Category.createCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleCategory $request)
    {
        $validated = $request->validated();
        $image = time() . '.' . $request->image->extension();
        $request->image->move(public_path("assets/images"), $image);
        $category = [
            'name' => $request->name,
            'slug' =>$request->slug,
            'image' => $image
        ];
        $this->categoryRepository->create($category);
        return redirect()->route('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);
        return view('Category.editCategory', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
        $category = $this->categoryRepository->find($id);
        if ($request->image != null) {
            $image = time() . '.' . $request->image->extension();
            $request->image->move(public_path("assets/images"), $image);
            $data = [
                'name' => $request->name,
                'slug' =>$request->slug,
                'image' => $image
            ];
            $this->categoryRepository->update($data, $category->id);
        } else {
            $data = [
                'name' => $request->name,
                'slug' =>$request->slug
            ];
            $this->categoryRepository->update($data, $category->id);
        }

        return redirect()->route('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);
        $this->categoryRepository->delete($category->id);
        return redirect()->route('category');
    }
}
