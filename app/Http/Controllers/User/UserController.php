<?php

namespace App\Http\Controllers\User;
use App\Http\Requests\RuleInputUser;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $userRepository;
    protected $categoryRepository;
    public function __construct(UserEloquentRepository $userRepository, CategoryEloquentRepository $categoryRepository)
    {
        $this ->userRepository = $userRepository;
        $this ->categoryRepository= $categoryRepository;
    }
    public function index()
    {
        $users=$this->userRepository->paginate(6);
        $categories = $this->categoryRepository->all();
        return view('User.UserDetails',[
            'users'=>$users,
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleInputUser $request)
    {
        $validated = $request->validated();
        $user = [
            'name' => $request -> name,
            'email' => $request ->email,
            'password' => Hash::make($request -> password),
            'phone' => $request -> phone,
        ];
        $this->userRepository->create($user);
        return redirect()->route('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('User.editUser',['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        $data=[
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => $request->role
        ];
        $this->userRepository->update($data,$user->id);
        return redirect()->route('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this -> userRepository -> find($id);
        $userId= $user -> id;
        $this->userRepository->delete($userId);
        return redirect()->route('user');
    }

    public function search(Request $request){
        $keyword= $request->keyword;
        $users=$this->userRepository->searchByName($keyword);
        return view('User.UserDetails',['users'=>$users]);
    }
}
