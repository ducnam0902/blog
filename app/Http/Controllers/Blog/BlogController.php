<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\PostEloquentRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $categoryRepository;
    protected $postRepository;
    public function __construct(CategoryEloquentRepository $categoryRepository, PostEloquentRepository $postRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }
    public function index()
    {
        $categories = $this->categoryRepository->all();
        foreach ($categories as $category) {
            $category_id = $category->id;
            break;
        }
        $posts = $this->postRepository->findByField('category_id', $category_id);
        $news = $this->postRepository->getLastestNew();

        return view('Blog.blog', ['categories' => $categories, 'posts' => $posts, 'news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $news = $this->postRepository->getLastestNew();
        $category = $this->categoryRepository->findBySlug($slug);
        $posts = $this->postRepository->findByField('category_id', $category->id);
        
        $myCollectionObj = collect($posts);
        $data= $this -> paginate($myCollectionObj);


        $categories = $this->categoryRepository->all();
        $data->withPath('/blog/'.$category->slug);
        //return $data;
        return view('Blog.blog', ['categories' => $categories, 'posts' => $data,'news' =>$news]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function paginate($items, $perPage = 4, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
