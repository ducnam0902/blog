<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\RulePost;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\PostEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PostController extends Controller
{
    protected $postRepository;
    protected $categoryRepository;
    public function __construct(PostEloquentRepository $postRepository, CategoryEloquentRepository $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository= $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->postRepository->paginate(4);
        $categories = $this->categoryRepository->all();
        return view('Post.postDetails', ['posts' => $posts, 'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories= $this->categoryRepository->all();
        return view('Post.createPost',[ 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RulePost $request)
    {
        //$validated = $request->validated();
        $image = time() . '.' . $request->image->extension();
        $request->image->move(public_path("assets/images"), $image);
        $post = [
            'title' => $request->title,
            'subtitle'=> $request->subtitle,
            'image' =>$image,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'slug' => $request->slug
            
        ];
        $this->postRepository->create($post);
        return redirect()->route('post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post= $this->postRepository->findBySlug($slug);
        $categories= $this->categoryRepository->all();
        $news = $this->postRepository->getLastestNew();
        return view('Post.detailPostClient',['post'=>$post, 'categories'=> $categories, 'news' =>$news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post= $this->postRepository->find($id);
        $categories= $this->categoryRepository->all();
        return view('Post.editPost',['post'=>$post,'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post= $this->postRepository->find($id);
        if ($request->image != null) {
            $image = time() . '.' . $request->image->extension();
            $request->image->move(public_path("assets/images"), $image);
            $data=[
                'title' => $request->title,
                'image' => $image,
                'subtitle'=> $request->subtitle,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'slug' => $request->slug
            ];
            $this-> postRepository->update($data,$post->id);
        } else {
            $data = [
                'title' => $request->title,
                'subtitle'=> $request->subtitle,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'slug' => $request->slug
            ];
            $this-> postRepository->update($data,$post->id);
        }
        
        return redirect()->route('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post= $this->postRepository->find($id);
        $this-> postRepository->delete($post->id);
        return redirect()->route('post');

    }
    public function search(Request $request){
        $keyword= $request->keyword;
        $posts= $this->postRepository->searchByName($keyword);
        return view('Post.postDetails',['posts'=>$posts]);
    }

    public function findBySlug($slug){
        $post= $this->postRepository->findBySlug($slug);
        $categories= $this->categoryRepository->all();
        $news = $this->postRepository->getLastestNew();
        return view('Post.detailPostClient',['post'=>$post, 'categories'=> $categories, 'news' =>$news]);
    }

    public function postFollowCategory($id){
        $categories= $this->categoryRepository->all();
        $data= $this->postRepository->findByField('category_id',$id);
        $dataArray= collect($data);
        $posts=  $this -> paginate($dataArray);
        $posts->withPath('/postFollowCategory/'.$id);
        return view('Post.postDetails',['posts'=>$posts, 'categories'=> $categories]);
    }

    public function paginate($items, $perPage = 4, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
