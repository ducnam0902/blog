<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\CategoryEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $categoryRepository;
    public function __construct( CategoryEloquentRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        
    }
    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('Admin.admin',['categories'=>$categories]);
    }
}
