<?php


namespace App\Repositories\Eloquent;


use App\Model\User;
use App\Repositories\BaseEloquentRepository;

class UserEloquentRepository extends BaseEloquentRepository
{

    public function model()
    {
        return User::class;
    }

    public function searchByName($keyword)
    {
        return User::where('name', 'LIKE', '%' . $keyword . '%')
                    ->orwhere('email','LIKE','%'.$keyword.'%')
                    ->paginate(5);
    }
}
