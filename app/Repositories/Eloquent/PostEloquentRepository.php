<?php


namespace App\Repositories\Eloquent;


use App\Model\Post;
use App\Repositories\BaseEloquentRepository;

class PostEloquentRepository extends BaseEloquentRepository{
    public function model()
    {
        return Post::class;
    }

    public function searchByName($keyword)
    {
        return Post::where('title', 'LIKE', '%' . $keyword . '%')
                        ->paginate(5);
    }
    public function getLastestNew(){
        return Post::orderBy('created_at','DESC') -> take(3)->get();
    }
    public function findBySlug($slug){
        return Post::where('slug',$slug)->first();
    }
    public function paginate($limit = null, $columns = array('*'))
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 20) : $limit;
        $results = Post::orderBy('id', 'ASC')->paginate($limit, $columns);
        $this->resetModel();

        return $results;
    }
}