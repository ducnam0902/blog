<?php


namespace App\Repositories\Eloquent;


use App\Model\category;
use App\Repositories\BaseEloquentRepository;

class CategoryEloquentRepository extends BaseEloquentRepository{
    public function model()
    {
        return category::class;
    }

    public function searchByName($keyword)
    {
        return category::where('name', 'LIKE', '%' . $keyword . '%')
                        ->paginate(5);
    }
    public function findBySlug($slug){
        return category::where('slug',$slug)->first();
    }
}