<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $fillable = [
        'id', 'name','image','slug'
    ];
    public function post(){
        return $this->hasMany(Post::class,'category_id');
    }
}
