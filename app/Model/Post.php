<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'id', 'title','subtitle','image','description','category_id','created_at','slug'
    ];
    public function category(){
        return $this->belongsTo(category::class,'category_id');
    }
}
