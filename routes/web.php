<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Blog\BlogController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Post\PostController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register',[AuthController::class,'register'])->name('register');
Route::post('/register',[AuthController::class,'postregister']);

Route::get('/login',[AuthController::class,'login'])->name('login');
Route::post('/login',[AuthController::class,'postlogin']);

Route::get('/logout',[AuthController::class,'logout'])->name('logout');

//Homepage for client
Route::get('/homepage',[HomeController::class,'index'])->name('home');

//Blog page follow category
Route::get('/blog/{slug}',[BlogController::class,'show'])->name('blogcategory');


//Post page 
Route::get('/post/{slug}',[PostController::class,'show'])->name('clientPost');

//change password
Route::get('/changepassword/{id}',[AuthController::class,'editPassword'])->name('changePassword');
Route::post('/changepassword/{id}',[AuthController::class,'updatePassword']);

Route::middleware(['auth', 'checkRole'])->group(function(){
    //Admin page
    Route::get('/',[AdminController::class,'index'])->name('index');

    //Details user
    Route::get('/userdetails',[UserController::class,'index'])->name('user');

    //Create user
    Route::get('/createUser',[UserController::class,'create'])->name("insertUser");
    Route::post('/createUser',[UserController::class,'store']);

    //Update user
    Route::get('/updateUser/{id}',[UserController::class,'edit'])->name("updateUser");
    Route::post('/updateUser/{id}',[UserController::class,'update']);

    //Delete user
    Route::get('/deleteUser/{id}',[UserController::class,'destroy'])->name('deleteUser');
    
    //Search User
    Route::get('/searchUser',[UserController::class,'search'])->name('searchUser');

    //Details Category
    Route::get('/categorydetails',[CategoryController::class,'index'])->name('category');

    //Create category
    Route::get('/createCategory',[CategoryController::class,'create'])->name('insertCategory');
    Route::post('/createcategory',[CategoryController::class,'store'])->name('uploadform');

    //Update category
    Route::get('/updatecategory/{id}',[CategoryController::class,'edit'])->name("updateCategory");
    Route::post('/updatecategory/{id}',[CategoryController::class,'update']);

    //Delete category
    Route::get('/deletecategory/{id}',[CategoryController::class,'destroy'])->name('deleteCategory');

    //Search category
    Route::get('/searchcategory',[CategoryController::class,'search'])->name('searchCategory');

    //Details Post
    Route::get('/postdetails',[PostController::class,'index'])->name('post');

    //Create Post
    Route::get('/createPost',[PostController::class,'create'])->name('insertpost');
    Route::post('/createpost',[PostController::class,'store'])->name('uploadformPost');

    //Update Post
    Route::get('/updatepost/{id}',[PostController::class,'edit'])->name("updatepost");
    Route::post('/updatepost/{id}',[PostController::class,'update']);

    //Delete Post
    Route::get('/deletepost/{id}',[PostController::class,'destroy'])->name('deletepost');

    //Search Post
    Route::get('/searchpost',[PostController::class,'search'])->name('searchpost');

    //Post follow category
    Route::get('/postFollowCategory/{id}',[PostController::class,'postFollowCategory'])->name('postFollow');

});
